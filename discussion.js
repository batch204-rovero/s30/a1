db.course_bookings.insertMany([
	{
		"courseId": "C001",
		"studentId": "S004",
		"isCompleted": true
	},
	{
		"courseId": "C002",
		"studentId": "S001",
		"isCompleted": false
	},
	{
		"courseId": "C001",
		"studentId": "S003",
		"isCompleted": true
	},
	{
		"courseId": "C003",
		"studentId": "S002",
		"isCompleted": false
	},
	{
		"courseId": "C001",
		"studentId": "S002",
		"isCompleted": true
	},
	{
		"courseId": "C004",
		"studentId": "S003",
		"isCompleted": false
	},
	{
		"courseId": "C002",
		"studentId": "S004",
		"isCompleted": true
	},
	{
		"courseId": "C003",
		"studentId": "S007",
		"isCompleted": false
	},
	{
		"courseId": "C001",
		"studentId": "S005",
		"isCompleted": true
	},
	{
		"courseId": "C004",
		"studentId": "S008",
		"isCompleted": false
	},
	{
		"courseId": "C001",
		"studentId": "S013",
		"isCompleted": true
	}
]);

/*
	Aggregation Pipeline Syntax:
		db.collectionName.aggregate([
			{Stage 1},
			{Stage 2},
			{Stage 3}
		])
*/

/*
	$group is used to group elements/documents together and create an analysis of these grouped documents

	Syntax:
		{
			$group: {_id: <id>, fieldResult: "valueResult"}
		}
*/

//Count all the documents in our database

db.course_bookings.aggregate([
	{
		$group: {
			_id: null, count: { $sum: 1}
		}
	}
]);

db.course_bookings.aggregate([
	{
		$group: {
			_id: "$studentId", count: { $sum: 1}
		}
	}
]);

/*
	$match is used to pass the documents or get the documents that will match our condition

	Syntax:
		{
			$match: {field: value}
		}
*/

db.course_bookings.aggregate([
	{
		$match: {
			"isCompleted": true
		}
	},
	{
		$group: {
			_id: "$courseId", total: {$sum: 1}
		}
	}
]);

/*
	$project will allow us to show or hide details

	Syntax:
		{
			$project: {field: 0 or 1}
		}
*/

db.course_bookings.aggregate([
	{
		$match: {
			"isCompleted": true
		}
	},
	{
		$project: {
			"courseId": 0,
			"studentId": 0
		}
	}
]);

db.course_bookings.count();

/*
	$sort can be used to change the order of the aggregated results

	Syntax:
		{
			$sort: {field: 1/1}
		}
*/

db.course_bookings.aggregate([
	{
		$match: {
			"isCompleted": true
		}
	},
	{
		$sort: {
			"courseId": -1
		}
	}
]);

db.course_bookings.aggregate([[
	{
		$match: {
			"isCompleted": true
		}
	},
	{
		$sort: {
			"courseId": -1
		}
	},
	{
		$sort: {
			"studentId": 1
		}
	}
]);

db.orders.insertMany([
	{
		"cust_Id": "A123",
		"amount": 500,
		"status": "A"
	},
	{
		"cust_Id": "A123",
		"amount": 250,
		"status": "A"
	},
	{
		"cust_Id": "B212",
		"amount": 200,
		"status": "A"
	},
	{
		"cust_Id": "B212",
		"amount": 200,
		"status": "D"
	}
]);

db.orders.aggregate([
	{
		$match: {
			"status": "A"
		}
	},
	{
		$group: {
			_id: "$cust_Id", total: {$sum: "$amount"}
		}
	}
]);

/*
	$sum
	$avg
	$min
	$max
*/

db.orders.aggregate([
	{
		$match: {
			"status": {
				$in: ["A", "D"]
			}
		}
	},
	{
		$group: {
			_id: "$cust_Id", maxAmount: {$max: "$amount"}
		}
	}
]);